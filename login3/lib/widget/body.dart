import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'contact.dart';

class Login1 extends StatelessWidget {
  const Login1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            Colors.red[400],
            Colors.red[300],
            Colors.red[200],
            Colors.red[100],
          ],
        ),
      ),
      child: Scaffold(
        // backgroundColor: kPrimaryLightColor,
        backgroundColor: Colors.transparent,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 120),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SvgPicture.asset(
                  "assets/icons/twitter.svg" ,
                  height: 100,
                  width: 100,
                  color: Colors.white,
                ),

                Text("Wellcome MizaNghiSon",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  color: Colors.white,
                   ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 30),
                  child: TextField(
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                        labelText: "UserName",
                        labelStyle: TextStyle(color: Colors.white,fontSize: 20,
                        ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0,0,0,40),
                  child: Stack(
                    alignment: AlignmentDirectional.centerEnd,
                    children: [
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            labelText: "Password",
                            labelStyle: TextStyle(color: Colors.white,fontSize: 20)
                        ),
                      ),
                      IconButton(icon: Icon(Icons.visibility,color: Colors.white,), onPressed: (){})
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                  child: SizedBox(
                    width: double.infinity,
                    height: 56,
                    child: RaisedButton(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                      onPressed: (){},
                      child: Text("Sign in",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),),
                    ),
                  ),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
