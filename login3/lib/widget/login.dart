import 'package:flutter/material.dart';

import 'contact.dart';
class login extends StatelessWidget {
  const login({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryLightColor,
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 200, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment: MainAxisAlignment.end,
            children: [

              Text("Hello\nWellcome Back", style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30
              ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                child: TextField(
                  decoration: InputDecoration(
                    labelText: "UserName",
                    labelStyle: TextStyle(color: Color(0xff888888),fontSize: 15)
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0,0,0,40),
                child: Stack(
                  alignment: AlignmentDirectional.centerEnd,
                  children: [
                     TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: "Password",
                            labelStyle: TextStyle(color: Color(0xff888888),fontSize: 15)
                        ),
                      ),
                    IconButton(icon: Icon(Icons.visibility), onPressed: (){})
                  ],
                ),
              ),
             Padding(
               padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
               child: SizedBox(
                 width: double.infinity,
                 height: 56,
                 child: RaisedButton(
                   color: kPiColor,
                     shape: RoundedRectangleBorder(
                       borderRadius: BorderRadius.all(Radius.circular(8)),
                     ),
                     onPressed: (){},
                         child: Text("sign in",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
                 ),
               ),
             )
            ],
          ),
        ),
      ),
    );
  }
}
