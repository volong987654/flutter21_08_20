import 'package:flutter/material.dart';
class Table_Data extends StatelessWidget {
  const Table_Data({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DataTable(
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Stt',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Chi Tiết',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              'Số Lượng',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ],
        rows: const <DataRow>[
          DataRow(
            cells: <DataCell>[
              DataCell(Text('1')),
              DataCell(Text('Tổng số xe ')),
              DataCell(Text('13')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('2')),
              DataCell(Text('Xe nhập')),
              DataCell(Text('10')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('3')),
              DataCell(Text('Xe suất')),
              DataCell(Text('3')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('4')),
              DataCell(Text('Chưa cân')),
              DataCell(Text('3')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('5')),
              DataCell(Text('KL nhập')),
              DataCell(Text('3')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('6')),
              DataCell(Text('Kl xuất')),
              DataCell(Text('3')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('7')),
              DataCell(Text('Kl tạp chất')),
              DataCell(Text('3')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('8')),
              DataCell(Text('Tiền nhập')),
              DataCell(Text('3')),
            ],
          ),
        ],
      ),
    );
  }
}
