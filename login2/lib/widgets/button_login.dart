import 'package:flutter/material.dart';

import '../contact.dart';

class LoginPage extends StatelessWidget {
  final String  text;
  final Function  press;
  final Color color, textColor;
  const LoginPage({
    Key key,
    this.text,
    this.press,
    this.color = kPiColor,
    this.textColor = Colors.white,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: size.width*0.85,
      height: size.height*0.07,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: GestureDetector(
        onTap: press(),
        child: Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w400,

          ),
        ),
      ),

    );
  }
}
