
import 'package:flutter/material.dart';
import 'package:login2/widgets/search.dart';
import 'package:login2/widgets/table_detail.dart';
import 'button_time_packer.dart';
import 'darttime_packer.dart';
class RecentOrders extends StatelessWidget {
  _buildRecentOrder(BuildContext context) {
    return InkWell(
      onTap: () => {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return Button_time_packer();
            },
          ),
        )
      },
      child: Container(
        width: 340,
        margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 1.0),
        // decoration: BoxDecoration(
        //   color: Colors.white,
        //   borderRadius: BorderRadius.circular(5.0),
        //   // border: Border.all(
        //   //   width: 1.0,
        //   //   color: Colors.grey[200],
        //   // ),
        // ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.grey),
          ),
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0,5,7,5),
              child: Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                    image: AssetImage("assets/images/nen10.jpg"),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  color: Colors.redAccent,
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                     "Miza Nghi Sơn",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                      ),
                      // overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5.0),
                    Text(
                      "KCN Nghi Sơn, Thanh Hóa",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
            child: Text(
              'Hiển thị danh sách',
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
           Padding(
             padding: const EdgeInsets.symmetric(vertical: 5),
             child: Center(
                child: Search(
                ),
              ),
           ),

          Container(
            height: size.height-247,
            width: size.width,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: 10,
              itemBuilder: (BuildContext context, int index) {
                return _buildRecentOrder(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}