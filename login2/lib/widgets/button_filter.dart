import 'package:flutter/material.dart';

import '../contact.dart';
class Button_Filter extends StatelessWidget {
  final String title;
  final String title1;
  final onClick;
  final color;

  Button_Filter({this.title, this.color = kPiColor, this.onClick, this.title1});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ButtonTheme(
      child: Container(
        width: size.width * 0.8,
        height: size.width * 0.1,
        child: RaisedButton(
          // color: this.color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
          onPressed: onClick,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 50 ),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 50 ),
                    child: Text(
                      "/",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    title1,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_drop_down,
                  color: Colors.white, size: 25.0),
            ],
          ),
        ),
      ),
    );
  }
}
