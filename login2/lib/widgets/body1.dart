import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login2/widgets/bottom_navigator.dart';
import 'package:login2/widgets/textfild_password.dart';
import 'button_login_new.dart';
import 'from_email.dart';
import 'home.dart';

class Body1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // It provide us total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.3),
                  width: size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(24),
                    topRight: Radius.circular(24),
                  )),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 50),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Đăng nhập",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        Fromlogin_Email(
                          hintext: "Sđt hoặc email",
                          onChanged: (value) {},
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Mật khẩu",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        FromLogin_Pasword(
                          hintext: "Mật khẩu",
                          onChanged: (value) {},
                        ),
                        SizedBox(height: size.height * 0.05),
                        ButtonWidget(
                          title: "Đăng nhập",
                          onClick: () => {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return List_Home();
                                },
                              ),
                            )
                          },),
                        // SizedBox(height: size.height * 0.04 ),
                        // OrDivider(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 150.0),
                  child: Center(
                    child: Column(
                      children: [
                        Text(
                          "Welcome to",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            "your MizaNghiSon",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 25),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Text(
                            "Let's log in to start!",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
