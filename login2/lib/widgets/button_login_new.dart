import 'package:flutter/material.dart';

import '../contact.dart';
class ButtonWidget extends StatelessWidget {
  final String title;
  final onClick;
  final color;

  ButtonWidget({this.title, this.color = kPiColor, this.onClick});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ButtonTheme(
      child: Container(
        width: size.width * 0.9,
        height: size.width * 0.13,
        child: RaisedButton(
          color: this.color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
          onPressed: onClick,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
