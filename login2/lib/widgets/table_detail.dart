import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:login2/widgets/table.dart';
import '../contact.dart';
import 'buttom_time.dart';
import 'button_filter.dart';
import 'button_login_new.dart';

class DatePickerWidget extends StatefulWidget {
  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  DateTimeRange dateRange;

  String getFrom() {
    if (dateRange == null) {
      return '00:00:2021';
    } else {
      return DateFormat('dd/MM/yyyy').format(dateRange.start);
    }
  }

  String getUntil() {
    if (dateRange == null) {
      return '00:00:2021';
    } else {
      return DateFormat('dd/MM/yyyy').format(dateRange.end);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("AUTOTIMELAPSE.COM"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "Miza Nghi Sơn",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
                   Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                          child: TextField(
                            decoration: InputDecoration(
                                labelText: "From",
                                labelStyle: TextStyle(color: kPibacgroundColor,fontSize: 17,fontWeight: FontWeight.bold),
                                hintText: "dd/MM/yyyy"
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                          child: TextField(
                            decoration: InputDecoration(
                                labelText: "To",
                                labelStyle: TextStyle(color: kPibacgroundColor,fontSize: 17,fontWeight: FontWeight.bold),
                                hintText: "dd/MM/yyyy"
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Button_Filter(
                            title: "Filter",
                            onClick: () => {},),
                        ),
                      ],
                    ),
                  ),
              Table_Data(),
            ],
          ),
        ),
      ),

    );
  }
  Future pickDateRange(BuildContext context) async {
    final initialDateRange = DateTimeRange(
      start: DateTime.now(),
      end: DateTime.now().add(Duration(hours: 24 * 3)),
    );
    final newDateRange = await showDateRangePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDateRange: dateRange ?? initialDateRange,
    );

    if (newDateRange == null) return;

    setState(() => dateRange = newDateRange);
  }
}
