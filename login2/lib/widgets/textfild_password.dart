import 'package:flutter/material.dart';
import 'package:login2/widgets/textfile_container.dart';


import '../contact.dart';

class FromLogin_Pasword extends StatelessWidget {
  final String hintext;
  final ValueChanged<String> onChanged;

  const FromLogin_Pasword({
    Key key, this.hintext,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFileldContainer(
      child: TextField(
        onChanged: onChanged,
        obscureText: true,
        decoration: InputDecoration(
            hintText: hintext,
            hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
            border: InputBorder.none
        ),
      ),
    );
  }
}

