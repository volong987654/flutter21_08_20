import 'package:flutter/material.dart';

import '../contact.dart';

class CustomListTile extends StatelessWidget {
  @override
  String title;
  String title1;
  Function onTap;
  CustomListTile(this.title,
      this.title1,
      this.onTap,);
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: kPiColor,
              ),
              borderRadius: BorderRadius.circular(5),
              color: kPiColor,
            ),
            height: 40,
            width: 180,
            // color: Colors.red,
            child: Row(
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    title,
                    style: TextStyle(
                        color: Colors.white, fontSize: 16,fontWeight: FontWeight.bold),
                  ),
                ),
                IconButton(
                    icon: Icon(Icons.arrow_drop_down,
                        color: Colors.white, size: 25.0),
                    // iconSize:50,
                    onPressed: onTap)
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.0,
                color: kPiColor,
              ),
              borderRadius: BorderRadius.circular(5),
              color: kPiColor,
            ),
            height: 40,
            width: 150,
            // color: Colors.red,
            child: Text(
              title1,
              style: TextStyle(
                  color: Colors.white, fontSize: 16,fontWeight: FontWeight.bold),
            ),
            alignment: Alignment.center,
          ),
        ],
      ),


    );
  }
}
