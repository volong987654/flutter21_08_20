import 'package:flutter/material.dart';

import '../contact.dart';
class Button_Detail extends StatelessWidget {
  final String title;
  final onPressed;
  final color;

  Button_Detail({this.title, this.color = kPiColor, this.onPressed});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ButtonTheme(
      child: Container(
        width: size.width * 0.35,
        height: size.width * 0.09,
        child: RaisedButton(
          color: this.color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
          onPressed: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Icon(Icons.arrow_drop_down,
                  color: Colors.white, size: 25.0),
            ],
          ),
        ),
        
      ),
    );
  }
}
