import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:intl/intl.dart';
import 'dart:async';

import 'button_detial.dart';

class NewTripDateView extends StatefulWidget {
  NewTripDateView({Key key}) : super(key: key);

  @override
  _NewTripDateViewState createState() => _NewTripDateViewState();
}

class _NewTripDateViewState extends State<NewTripDateView> {
  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now().add(Duration(days: 7));

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: _startDate,
        initialLastDate: _endDate,
        firstDate: new DateTime(DateTime.now().year - 50),
        lastDate: new DateTime(DateTime.now().year + 50));
    if (picked != null && picked.length == 2) {
      setState(() {
        _startDate = picked[0];
        _endDate = picked[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('AUTOTIMELAPSE.COM'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: SafeArea(
                child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Text(
              "Miza Nghi Sơn",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Button_Detail(
                      title: "From",
                      onPressed: () async {
                        await displayDateRangePicker(context);
                      },
                    ),
                    Text("Start Date: ${DateFormat('dd/MM/yyyy').format(_startDate).toString()}"),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Button_Detail(
                      title: "To",
                      onPressed: () async {
                        await displayDateRangePicker(context);
                      },
                    ),
                    Text("End Date: ${DateFormat('dd/MM/yyyy').format(_endDate).toString()}"),
                  ],
                ),
              ],
            ),
        ]))));
  }
}
//         body: Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 Button_Detail(
//                   title:"Select Dates",
//                   onPressed: () async {
//                     await displayDateRangePicker(context);
//                   },
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//                     Text("Start Date: ${DateFormat('MM/dd/yyyy').format(_startDate).toString()}"),
//                     Text("End Date: ${DateFormat('MM/dd/yyyy').format(_endDate).toString()}"),
//                   ],
//                 ),
//               ],
//             ))

//   };
//   }
// }
