import 'package:flutter/material.dart';
import 'package:login2/widgets/table.dart';

import 'button_filter.dart';
import 'button_login_new.dart';
class Button_time_packer extends StatelessWidget {
  const Button_time_packer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AUTOTIMELAPSE.COM"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "Miza Nghi Sơn",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Button_Filter(
                      title:"From",
                      title1: "to",
                        onClick: () => {},
                    ),

                    // ButtonWidget(
                    //   title: "Đăng nhập",
                    //   onClick: () => {
                    //     Navigator.pushReplacement(
                    //       context,
                    //       MaterialPageRoute(
                    //         builder: (context) {
                    //           return List_Home();
                    //         },
                    //       ),
                    //     )
                    //   },),
                  ],
                ),
              ),
              Table_Data(),
            ],
          ),
        ),
      ),

    );
  }
}

