import 'package:flutter/material.dart';

import '../contact.dart';


class TextFileldContainer extends StatelessWidget {
  final Widget child;
  const TextFileldContainer({
    Key key, this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top:5),
      width: size.width*0.9,
      height: size.height*0.07,
      padding: EdgeInsets.symmetric(horizontal: 20,vertical: 5),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.3),
        borderRadius: BorderRadius.circular(15),
      ),
      child: child,
    );
  }
}
