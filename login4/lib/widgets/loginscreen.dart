import 'package:flutter/material.dart';
import 'package:login4/widgets/textfild_container.dart';

import '../contact.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 200),
                child: SizedBox(
                  height: 200.0,
                  width: 200,
                  child: Image.asset(
                    "assets/images/logo.png",
                    fit: BoxFit.cover
                    ,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                child: TextField(
                  decoration: InputDecoration(
                      labelText: "UserName",
                      labelStyle: TextStyle(color: kPibacgroundColor,fontSize: 17,fontWeight: FontWeight.bold)
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 10),
                child: TextField(
                  decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(color: kPibacgroundColor,fontSize: 17,fontWeight: FontWeight.bold)
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              LoginPage(
                text: "Đăng nhập",
                press: (){},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
