import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login/widgets/button_login.dart';
import 'package:login/widgets/button_login1.dart';
import 'package:login/widgets/email_from_new.dart';
import 'package:login/widgets/from_email.dart';
import 'package:login/widgets/textfild_password.dart';

import '../contact.dart';
class Login1 extends StatelessWidget {
  const Login1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.95),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 120),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [

               SvgPicture.asset(
                    "assets/icons/twitter.svg" ,
                    height: 50,
                    width: 50,
                  color: kPiColor,
                ),
                    SizedBox(
                      height: 50,
                    ),
                    Text("Login to Miza NghiSon",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
              SizedBox(
                height: 10,
              ),
                    Text("We cover all transportations",style: TextStyle(fontSize: 16),),
              SizedBox(
                height: 50,
              ),
              Fromlogin_Email(
                hintext: "Vo Long",
                onChanged: (value){
                },
              ),
              SizedBox(
                height: 20,
              ),
              FromLogin_Pasword(
                hintext: "Password",
                onChanged: (value){},
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: LoginPage1(
                  text: "Login",
                  press: (){},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
