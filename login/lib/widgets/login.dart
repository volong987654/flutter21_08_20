import 'package:flutter/material.dart';
import 'package:login/widgets/body.dart';

import '../contact.dart';
class Login extends StatelessWidget {
  const Login({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/nen6.jpeg"), fit: BoxFit.cover,),
              ),
            ),
            new Center(
              child: Body(),
            )
          ],
        )
    );
  }
}
