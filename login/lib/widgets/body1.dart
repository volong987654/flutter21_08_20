import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login/login/login1.dart';
import 'package:login/widgets/textfild_password.dart';

import 'button_login.dart';
import 'from_email.dart';
import 'or_drive.dart';

class Body1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // It provide us total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: size.height *0.3),
                  width: size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(24),
                        topRight: Radius.circular(24),
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                            Text("Đăng nhập",style: TextStyle(fontWeight: FontWeight.bold),),
                            Fromlogin_Email(
                              hintext: "Sđt hoặc email",
                              onChanged: (value){
                              },
                            ),
                        SizedBox(
                          height: 20,
                        ),
                        Text("Mật khẩu",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                        FromLogin_Pasword(
                          hintext: "Mật khẩu",
                          onChanged: (value){},
                        ),
                        SizedBox(height: size.height * 0.05 ),
                        Center(
                          child: LoginPage(
                            text: "Đăng nhập",
                            press: (){},
                          ),
                        ),
                        // SizedBox(height: size.height * 0.04 ),
                        // OrDivider(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:120.0),
                  child: Center(
                    child: Column(
                        children: [
                          Text("Welcome to",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
                          Text("your MizaNghiSon",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
                        ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

