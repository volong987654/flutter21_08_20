import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'contaier_filed.dart';

class Login_Email extends StatelessWidget {
  final String  iconSrc;
  final String hintext;
  final IconData icon;
  final ValueChanged<String> onChanged;
  Login_Email({
    Key key,
    this.hintext,
    this.icon = Icons.person,
    this.onChanged, this.iconSrc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Contextfile(
      child: TextField(
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: 'Email or Phone',
          contentPadding: EdgeInsets.only(top: 15),
          hintStyle: TextStyle(
            fontSize: 15,
          ),
        ),
      ),

    );
  }
}