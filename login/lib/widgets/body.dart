import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:login/login/login1.dart';
import 'package:login/widgets/textfild_password.dart';

import 'button_login.dart';
import 'from_email.dart';
import 'or_drive.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // It provide us total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
         SizedBox(
           height: size.height,
           child: Stack(
             children: <Widget>[
               Container(
                 margin: EdgeInsets.only(top: size.height *0.3),
                 width: size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24),
                    )
                  ),
                 child: Column(
                   children: <Widget>[
                     Padding(
                       padding: const EdgeInsets.only(top:60.0),
                       child: Text("Đăng nhập",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),),
                     ),
                     Fromlogin_Email(
                       hintext: "Sđt hoặc email",
                       onChanged: (value){
                       },
                     ),
                     FromLogin_Pasword(
                       hintext: "Mật khẩu",
                       onChanged: (value){},
                     ),
                     SizedBox(height: size.height * 0.05 ),
                     LoginPage(
                       text: "Đăng nhập",
                       press: (){},
                     ),
                     Padding(
                       padding: const EdgeInsets.only(top:20.0),
                       child: Text("Quên mật khẩu",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                     ),
                     // SizedBox(height: size.height * 0.04 ),
                     // OrDivider(),
                   ],
                 ),
               ),
               Padding(
                 padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 70),
                 child: Column(
                   children: [
                     Text("Welcome",style: TextStyle(color: Colors.white,fontSize: 15),),
                     Text("Miza NghiSon",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 25),),
                   ],
                 ),
               )
             ],
           ),
         ),
        ],
      ),
    );
  }
}

