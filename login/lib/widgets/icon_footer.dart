import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../contact.dart';

class IconFooter extends StatelessWidget {
  final String iconSrc;
  final Function press;
  const IconFooter({
    Key key,
    this.iconSrc,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: press(),
      child: Container(
        margin: EdgeInsets.only(left:size.height*0.01,top:20),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: kPibacgroundColor.withOpacity(0.5),
          ),
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(
          iconSrc ,
          height: 20,
          width: 20,),
      ),
    );
  }
}




