
import 'package:flutter/material.dart';
import 'package:login/widgets/textfile_container.dart';

import '../contact.dart';

class Fromlogin_Email extends StatelessWidget {
  final String hintext;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const Fromlogin_Email({
    Key key,
    this.hintext,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFileldContainer(
      child: TextField(
        onChanged: onChanged,
        decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.only(top:10.0,right: 15),
            child: Text("EnterID",style: TextStyle(color: Colors.grey.withOpacity(0.5),fontWeight: FontWeight.bold,fontSize: 16),),
          ),
          hintText: hintext,
          hintStyle: TextStyle(color: Colors.black,fontWeight: FontWeight.w900,fontSize: 16),
          contentPadding: EdgeInsets.only(top:9),
          border: InputBorder.none,
        ),
      ),
    );
  }
}