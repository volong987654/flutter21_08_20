import 'package:flutter/material.dart';
import 'package:login/widgets/textfile_container.dart';

import '../contact.dart';

class FromLogin_Pasword extends StatelessWidget {
  final String hintext;
  final IconData icon1;
  final IconData icon2;
  final ValueChanged<String> onChanged;

  const FromLogin_Pasword({
    Key key, this.hintext,
    this.icon1 = Icons.lock,
    this.icon2 = Icons.visibility,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFileldContainer(
      child: TextField(
        onChanged: onChanged,
        obscureText: true,
        decoration: InputDecoration(
            hintText: hintext,
            hintStyle: TextStyle(color: Colors.grey.withOpacity(0.5),fontWeight: FontWeight.bold,fontSize: 16),
            border: InputBorder.none
        ),
      ),
    );
  }
}

